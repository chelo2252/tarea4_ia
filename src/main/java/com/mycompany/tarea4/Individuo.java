/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.tarea4;

import java.io.Serializable;
import  java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
/**
 *
 * @author Michael
 */
public class Individuo implements Serializable, Comparable<Individuo>  {
    ArrayList<Integer> cromosoma;
    Double fitness;
    Double eval1;
    Double eval2;

    public Individuo(int n) {
        this.fitness = Double.MAX_VALUE;
        this.eval1 = Double.MAX_VALUE;
        this.eval2 = Double.MAX_VALUE;
        
        ArrayList<Integer> instalacionesDisponibles = new ArrayList<>();
        for (int i = 0; i < n; i++) 
            instalacionesDisponibles.add(i);
        
        cromosoma = new ArrayList<>();
        for (int i = 0; i < n; i++)
            cromosoma.add(pickRandom(instalacionesDisponibles));
    }

    public Individuo(Hormiga h) {
        
        this.cromosoma = new ArrayList<>();
        for (int i = 0; i < h.getCamino().length; i++) 
            this.cromosoma.add(h.getCamino()[i]);
        
        this.eval1 = h.getEvaluacion1();
        this.eval2 = h.getEvaluacion2();
    }
    
    
    
    public ArrayList<Integer> getCromosoma() {
        return cromosoma;
    }

    public void setCromosoma(ArrayList<Integer> cromosoma) {
        this.cromosoma = cromosoma;
    }

    public Double getFitness() {
        return fitness;
    }

    public void setFitness(Double fitness) {
        this.fitness = fitness;
    }
    
    public int distancia(Individuo ind) {
        ArrayList<Integer> otroCromosoma = ind.getCromosoma();
        int dist = cromosoma.size();
        for (int i = 0; i < cromosoma.size(); i++) {
            if (cromosoma.get(i)== otroCromosoma.get(i))
                dist--;
        }
        return dist;
    }

    @Override
    public int compareTo(Individuo o) {
        return (int)(this.fitness - o.fitness);
    }
    
    public void mutar() {
        int tamanio = cromosoma.size();
        Random r = new Random();

        int ptoCorte1 = r.nextInt(tamanio),
            ptoCorte2 = r.nextInt(tamanio);

        while (ptoCorte1 == ptoCorte2) {
                ptoCorte2 = r.nextInt(tamanio);
        }

        Collections.swap(cromosoma, ptoCorte1, ptoCorte2);
    }
    
    private int pickRandom(ArrayList<Integer> instalaciones) {
        if (instalaciones.isEmpty()) return -1;
        int rand = (int) (Math.random()*instalaciones.size());
        int instalacion = instalaciones.get(rand);
        instalaciones.remove(rand);
        return instalacion;
    }

    @Override
    public boolean equals(Object otro) {
        for (int i = 0; i < cromosoma.size(); i++) 
            if (cromosoma.get(i) != ((Individuo)otro).getCromosoma().get(i))
                return false;
            
        return true;
    }

    public Double getEval1() {
        return eval1;
    }

    public void setEval1(Double eval1) {
        this.eval1 = eval1;
    }

    public Double getEval2() {
        return eval2;
    }

    public void setEval2(Double eval2) {
        this.eval2 = eval2;
    }
    
    public boolean cubre(Individuo i2){
        return (this.eval1<=i2.eval1 && this.eval2<=i2.eval2);
    }
}
