/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.tarea4;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 
 * @author Marcelo Cardozo
 */
public class Instancia {
    public static final int CANT_LOCALID = 0;
    public static final int FLUJO_1 = 1;
    public static final int FLUJO_2 = 2;
    public static final int DISTANCIA = 3;
    
    private Integer[][][] flujo;
    private Integer[][] flujo1;
    private Integer[][] flujo2;
    private Integer[][] distancia;
    private int n;
    
    private String nombre;
    
    public Instancia(String instancia){
        nombre = instancia;
        BufferedReader in = null;
        try {
            in = new BufferedReader(new FileReader(instancia));
            
            n = Integer.parseInt(in.readLine());
            flujo1 = new Integer[n][n];
            flujo2 = new Integer[n][n];
            distancia = new Integer[n][n];

            int tipoValor = 1;
            int i = 0;
            String line;
            while((line = in.readLine()) != null){
                if(line.trim().isEmpty()){
                    tipoValor++;
                    i = 0;
                    continue;
                }

                Pattern p = Pattern.compile("[0-9]{1,3}");
                Matcher m = p.matcher(line);

                switch(tipoValor){
                    case FLUJO_1:
                        for(int j = 0; m.find() ;j++){
                            flujo1[i][j] = Integer.parseInt(m.group());
                        }
                        break;
                    case FLUJO_2:
                        for(int j = 0; m.find() ;j++){
                            flujo2[i][j] = Integer.parseInt(m.group());
                        }
                        break;
                    case DISTANCIA:
                        for(int j = 0; m.find() ;j++){
                            distancia[i][j] = Integer.parseInt(m.group());
                        }
                        break;
                }
                i++;
            }
            
        } catch (Exception e) {
        } finally{
            if(in != null)
                try {
                    in.close();
                } catch (Exception e) {
                }
                
        }
        flujo = new Integer[2][n][n];
        flujo[0] = flujo1;
        flujo[1] = flujo2;
    }

    public Integer[][][] getFlujo() {
        return flujo;
    }

    public void setFlujo(Integer[][][] flujo) {
        this.flujo = flujo;
    }

    public Integer[][] getFlujo1() {
        return flujo1;
    }

    public void setFlujo1(Integer[][] flujo1) {
        this.flujo1 = flujo1;
    }

    public Integer[][] getFlujo2() {
        return flujo2;
    }

    public void setFlujo2(Integer[][] flujo2) {
        this.flujo2 = flujo2;
    }

    public Integer[][] getDistancias() {
        return distancia;
    }

    public void setDistancia(Integer[][] distancia) {
        this.distancia = distancia;
    }

    public int getN() {
        return n;
    }

    public void setN(int n) {
        this.n = n;
    }
    
    public int getDistancia(int ubi1, int ubi2) {
        return distancia[ubi1][ubi2];
    }
    
    public int getFlujo1(int instalacion1, int instalacion2) {
        return flujo1[instalacion1][instalacion2];
    }
    
    public int getFlujo2(int instalacion1, int instalacion2) {
        return flujo2[instalacion1][instalacion2];
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
