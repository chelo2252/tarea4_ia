/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.tarea4;

import java.util.HashMap;

/**
 * 
 * @author Marcelo Cardozo
 */
public class Feromona {
    HashMap<String, HashMap<String, Double>> tabla;
    int cantUbicaciones;
    int cantEdificios;
    double taoInicial;
    public Feromona(int cantUbicaciones, int cantEdificios){
        tabla = new HashMap<>();
        this.cantUbicaciones = cantUbicaciones;
        this.cantEdificios = cantEdificios;
        
        //resetFeromonas(null);
    }
    
    public Feromona(int cantUbicaciones, int cantEdificios, double tao){
        tabla = new HashMap<>();
        this.cantUbicaciones = cantUbicaciones;
        this.cantEdificios = cantEdificios;
        this.taoInicial = tao;
        //resetFeromonas(tao);
    }
    
    public String getCodigo(int ubicacion, int edificio){
        return ubicacion+";"+edificio;
    }
    
    public void resetFeromonas(Double t){
        /*for(int i = 0; i < cantUbicaciones; i++)
            for(int j = 0 ; j< cantEdificios;j++)
                for(int k = 0; i < cantUbicaciones; k++)
                    for(int l = 0 ; j< cantEdificios;l++)
                        setValue(i, j, k, l, t);*/
        for(String key : tabla.keySet())
            for(String key2 : tabla.get(key).keySet())
                tabla.get(key).put(key2, t);
    }
        
    public double getValue(int ubicacionOrigen, int edificioOrigen, int ubicacionDestino, int edificioDestino){
        String k1 = getCodigo(ubicacionOrigen, edificioOrigen);

        HashMap<String,Double> tablaOrigen = tabla.get(k1);
        if(tablaOrigen == null){
            tablaOrigen = new HashMap<>();
            tabla.put(k1, tablaOrigen);
        }
        String k2 = getCodigo(ubicacionDestino, edificioDestino);
        Double valor = tablaOrigen.get(k2);        
        if(valor == null){
            setValue(ubicacionOrigen, edificioOrigen, ubicacionDestino, edificioDestino, taoInicial);
            return taoInicial;
        }else
            return valor;
    }
    
    public void setValue(int ubicacionOrigen, int edificioOrigen, int ubicacionDestino, int edificioDestino, double valor){
        String k1 = getCodigo(ubicacionOrigen, edificioOrigen);
        HashMap<String,Double> tablaOrigen = tabla.get(k1);
        if(tablaOrigen == null){
            tablaOrigen = new HashMap<>();
            tabla.put(k1, tablaOrigen);
        }
        
        tablaOrigen.put(getCodigo(ubicacionDestino, edificioDestino),valor);        
    }
}
