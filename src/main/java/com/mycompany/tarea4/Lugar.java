/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.tarea4;

/**
 * 
 * @author Marcelo Cardozo
 */
public class Lugar {
    private int posUbicacion;
    private int ubicacion;
    
    private int posEdificio;
    private int edificio;

    public Lugar(int ubicacion, int edificio) {
        this.ubicacion = ubicacion;
        this.edificio = edificio;
    }

    public int getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(int ubicacion) {
        this.ubicacion = ubicacion;
    }

    public int getEdificio() {
        return edificio;
    }

    public void setEdificio(int edificio) {
        this.edificio = edificio;
    }

    public int getPosUbicacion() {
        return posUbicacion;
    }

    public void setPosUbicacion(int posUbicacion) {
        this.posUbicacion = posUbicacion;
    }

    public int getPosEdificio() {
        return posEdificio;
    }

    public void setPosEdificio(int posEdificio) {
        this.posEdificio = posEdificio;
    }

    public Lugar(int posUbicacion, int ubicacion, int posEdificio, int edificio) {
        this.posUbicacion = posUbicacion;
        this.ubicacion = ubicacion;
        this.posEdificio = posEdificio;
        this.edificio = edificio;
    }
    
    
}
