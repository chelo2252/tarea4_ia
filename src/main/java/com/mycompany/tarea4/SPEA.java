/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.tarea4;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Random;
import java.util.Set;

/**
 *
 * @author Michael
 */
public class SPEA {
    private Instancia instancia;
    private double mutacion;
    private int maxPareto;
    private int maxGeneraciones;
    private int tamPoblacion;
    private Set<Individuo> pareto;
    private Set<Individuo> poblacion;
    
    ArrayList<Individuo> selectionPool;
    ArrayList<Individuo> matingPool;
    
    public SPEA(Instancia instancia, int tamPoblacion,int maxPareto, int maxGeneraciones, double mutacion) {
        this.instancia = instancia;
        this.maxPareto = maxPareto;
        this.mutacion = mutacion;
        this.maxGeneraciones = maxGeneraciones;
        this.tamPoblacion = tamPoblacion;
    }

    public Set<Individuo> getPareto() {
        return pareto;
    }

    public void setPareto(Set<Individuo> pareto) {
        this.pareto = pareto;
    }
    
    public void run() {
        init();
        for (int i = 0; i < maxGeneraciones; i++) {
            actualizarPareto();
            asignarFitness();
            seleccion();
            aplicarOperadoresGeneticos();
        }
    }
    
    private void init() {
        poblacion = new HashSet<Individuo>();
        for (int i = 0; i < tamPoblacion; i++)
            poblacion.add(new Individuo(instancia.getN()));
        
        pareto = new HashSet<Individuo>();
    }
    
    private void actualizarPareto() {
        for (Individuo ind : poblacion) {
            if (noDominada(ind)) {
                mergePareto(ind);
                pareto.add(ind);
                if (pareto.size() > maxPareto) {
                    //reducirPareto();
                }
            }
        }
    }
    
    private boolean noDominada(Individuo ind) {
        if (pareto.isEmpty()) return true;
        for (Individuo indPareto : pareto)
            if (domina(indPareto, ind))
                return false;
        return true;
    }
    
    private void mergePareto(Individuo nuevoIndPareto) {
        Iterator it = pareto.iterator();
        while (it.hasNext()){
           Individuo indPareto = (Individuo) it.next();
           if (domina(nuevoIndPareto, indPareto))
                it.remove();
        }
//        for (Individuo indPareto : pareto) 
//            if (domina(nuevoIndPareto, indPareto))
//                pareto.remove(indPareto);
        
        
    }
    
    private void reducirPareto() {
        Set<Individuo> clusterSet = new HashSet<>();
        for (Individuo individuoPareto : pareto) {
            clusterSet.add(individuoPareto);
        }
        
        while (clusterSet.size() > maxPareto) {            
            int minDist = Integer.MAX_VALUE;
            for (Individuo cluster1 : clusterSet) {
                for (Individuo cluster2 : clusterSet) {
                    if (!cluster1.equals(cluster2)) {
                        int dist = cluster1.distancia(cluster2);
                        //if (dist < minDist)
                    }
                }
            }
        }
    }
    
    private void asignarFitness() {
        //Asignar strength a c/ miembro del pareto, q es a la vez su fitness
        for (Individuo individuoPareto : pareto) {
            int conttador = 0;
            for (Individuo individuoPoblacion : poblacion) {
                if (domina(individuoPareto, individuoPoblacion)) conttador++;
            }
            double strength = conttador/(poblacion.size()+1);
            individuoPareto.setFitness(strength);
        }
        
        //Asignar fitness a c miembro de la poblacion
        for (Individuo individuoPoblacion : poblacion) 
            individuoPoblacion.setFitness(fitnessPoblacion(individuoPoblacion));
    }
    
    private boolean domina(Individuo ind1, Individuo ind2) {
        if (ind1.getEval1() == Double.MAX_VALUE) ind1.setEval1(evalObjetivo1(ind1));
        if (ind1.getEval2() == Double.MAX_VALUE) ind1.setEval2(evalObjetivo2(ind1));
        if (ind2.getEval1() == Double.MAX_VALUE) ind2.setEval1(evalObjetivo1(ind2));
        if (ind2.getEval2() == Double.MAX_VALUE) ind2.setEval2(evalObjetivo2(ind2));
        return !ind1.equals(ind2) 
                && ind1.getEval1() <= ind2.getEval1()
                && ind1.getEval2() <= ind2.getEval2();
    }
    
    private double evalObjetivo1(Individuo ind) {
        double eval = 0;
        ArrayList<Integer> cromosoma = ind.getCromosoma();
        
        for (int i = 0; i < cromosoma.size(); i++) 
            for (int j = 0; j < cromosoma.size(); j++) 
                    if (i != j) {
                        int instalacion1 = cromosoma.get(i);
                        int instalacion2 = cromosoma.get(j);
                        eval += instancia.getDistancia(i, j) * instancia.getFlujo1(instalacion1, instalacion2);
                    }
        return eval;
    }
    
    private double evalObjetivo2(Individuo ind) {
        double eval = 0;
        ArrayList<Integer> cromosoma = ind.getCromosoma();
        
        for (int i = 0; i < cromosoma.size(); i++) 
            for (int j = 0; j < cromosoma.size(); j++) 
                    if (i != j) {
                        int instalacion1 = cromosoma.get(i);
                        int instalacion2 = cromosoma.get(j);
                        eval += instancia.getDistancia(i, j) * instancia.getFlujo2(instalacion1, instalacion2);
                    }
        return eval;
    }
    
    private void seleccion() {
        matingPool = new ArrayList<>();
        selectionPool = new ArrayList<>();
        selectionPool.addAll(poblacion);
        selectionPool.addAll(pareto);
        
        // n/2 binary tournaments
        for (int i = 0; i < poblacion.size()/2; i++) {
            Individuo ind1 = pickRandom(selectionPool);
            Individuo ind2 = pickRandom(selectionPool);
            Individuo winner = (ind1.getFitness() > ind2.getFitness())? ind1 : ind2;
            matingPool.add(winner);
        }
    }
    
    private void aplicarOperadoresGeneticos() {
        
        for (int i = 0; i < matingPool.size(); i+=2) {
            Individuo padre1 = matingPool.get(i);
            Individuo padre2 = matingPool.get(i+1);
            
            int tamanio = instancia.getN();
            Random r = new Random();
            int ptoCorte1 = r.nextInt(tamanio - 1);
            int ptoCorte2 = r.nextInt(tamanio - ptoCorte1) + ptoCorte1;

            if (ptoCorte1 == ptoCorte2)
                    ptoCorte2++;
            
            //Crossover
            Individuo hijo1 = cruzar(padre1, padre2, ptoCorte1, ptoCorte2),
                      hijo2 = cruzar(padre2, padre1, ptoCorte1, ptoCorte2);
            
            
            poblacion.add(hijo1);
            poblacion.add(hijo2);
            
        }
        
        //Mutacion
        for (Individuo ind : poblacion) {
            if (Math.random() < mutacion ) {
                ind.mutar();
                //recalcular su fitness
                ind.setFitness(fitnessPoblacion(ind));
            }
        }
        
        // Ordenamos la poblacion respecto al fitness y eliminamos los que sobren
        ArrayList<Individuo> nuevaPoblacion = new ArrayList<>(poblacion);
        Collections.sort(nuevaPoblacion);
        while(nuevaPoblacion.size() > tamPoblacion)
            nuevaPoblacion.remove(nuevaPoblacion.size()-1);
        poblacion = new HashSet<>(nuevaPoblacion);
    }
    
    private Individuo pickRandom(ArrayList<Individuo> selectionPool) {
        if (selectionPool.isEmpty()) return null;
        int rand = (int) (Math.random()*selectionPool.size());
        return selectionPool.get(rand);
    }
    
    private Individuo cruzar(Individuo padre1, Individuo padre2, int ptoCorte1, int ptoCorte2) {
        int tamanio = padre1.getCromosoma().size();
        Individuo nuevo = new Individuo(tamanio);

        for (int i = ptoCorte1; i <= ptoCorte2; i++)
                nuevo.getCromosoma().set(i, padre1.getCromosoma().get(i));

        int i = ptoCorte1, 
            j = 0;
        
        while ( nuevo.getCromosoma().contains(null) ) {
                if (!nuevo.getCromosoma().contains(padre2.getCromosoma().get(i)) 
                        && nuevo.getCromosoma().get(j) == null) {
                        nuevo.getCromosoma().set(j, padre2.getCromosoma().get(i));
                        j++;
                        i = (i+1)%tamanio;
                }
                else if (nuevo.getCromosoma().get(j)!=null) {
                        j++;
                }
                else {
                        i = (i+1)%tamanio;
                }
        }
        return nuevo;
    }
    
    private double fitnessPoblacion(Individuo ind) {
        double strengthsSum = 0;
        for (Individuo individuoPareto : pareto) {
            if (domina(individuoPareto, ind)) 
                strengthsSum += individuoPareto.getFitness();
        }

        return strengthsSum + 1;
    }

}
