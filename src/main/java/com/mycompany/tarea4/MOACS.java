/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.tarea4;

import java.util.ArrayList;
import java.util.HashSet;

/**
 * 
 * @author Marcelo Cardozo
 */
public class MOACS {
    private Instancia instancia;
    
    int cantHormigas;
    
    Feromona tablaFeromona;
    double beta;
    double q0;
    double coefEvaporacion;
    
    double taoInicial;
    
    double tao;
    
    HashSet<Hormiga> pareto;
    
    int n;
    
    public MOACS(Instancia instancia, int cHormigas){
        init(instancia, cHormigas);
    }
    
    public void reset(){
        init(instancia, cantHormigas);
    }
    
    public void init(Instancia instancia, int cHormigas){
        this.instancia = instancia;
        inicializarParametros();
        
        this.pareto = new HashSet<>();
        this.tablaFeromona = new Feromona(n,n,taoInicial);
        
        this.cantHormigas = cHormigas;
    }
    
    private void inicializarParametros(){
        this.beta = 4;
        this.coefEvaporacion = 0.3;
        this.taoInicial = 0.3;
        this.q0 = 0.4;
        this.n = instancia.getN();
    }
    
    public void doWork(){
        
        int generacion = 0;
        while (!condicionParada(generacion)) {                    
            Hormiga[] hormigas = new Hormiga[cantHormigas];
            for(int i = 0; i< hormigas.length ; i++){
                hormigas[i] = new Hormiga(n);
            }
                
            for(int nroHormiga = 0; nroHormiga < cantHormigas; nroHormiga++){
                construirSolucion(hormigas[nroHormiga],nroHormiga);
                evaluarSolucion(hormigas[nroHormiga]);
                actualizarConjuntoPareto(hormigas[nroHormiga]);
            }
            
            double[] avr = calcularParetoAverage();
            double taoPrima = calcularTaoPrima(avr);

            if (taoPrima > tao) {
                tao = taoPrima;
                tablaFeromona.resetFeromonas(tao);
            } else {
                for(Hormiga h : pareto){
                    actualizarFeromonasGlobal(h,avr);
                }
            }
            generacion++;
        }
        
        
    }
    
    private boolean condicionParada(int generacion){
        return generacion == 50;
    }
    
    private void construirSolucion(Hormiga hormiga, int nroHormiga){
        int[] camino = new int[n];
        for(int i = 0; i < camino.length ; i++)
            camino[i] = -1;
        
        hormiga.setCamino(camino);
        
        ArrayList<Integer> ubicacionesLibres = new ArrayList<>();
        ArrayList<Integer> edificiosLibres = new ArrayList<>();
        
        for(int i = 0; i < n ; i++){
            ubicacionesLibres.add(i);
            edificiosLibres.add(i);
        }
        
        
        int posUbicacion = (int)(Math.random()*ubicacionesLibres.size());
        int ubicacion = ubicacionesLibres.get(posUbicacion);
        ubicacionesLibres.remove(posUbicacion);
        
        
        int posEdificio = (int)(Math.random()*edificiosLibres.size());
        camino[ubicacion] = edificiosLibres.get(posEdificio);
        edificiosLibres.remove(posEdificio);
        
        
        Lugar origen = new Lugar(posUbicacion,ubicacion, posEdificio, camino[ubicacion]);
        
        while(existenEstadosNoVisitados(ubicacionesLibres)){
            Lugar destino = seleccionarSiguienteEstado(origen, nroHormiga,ubicacionesLibres, edificiosLibres);
            camino[destino.getUbicacion()] = destino.getEdificio();
            
            ubicacionesLibres.remove(destino.getPosUbicacion());
            edificiosLibres.remove(destino.getPosEdificio());
            
            actualizarFeromonasPasoPaso(origen,destino);
            
            origen = destino;
        }
    }
    
    private void evaluarSolucion(Hormiga hormiga){
        int[] camino = hormiga.getCamino();
        
        int suma = 0;
        for(int i = 0; i < camino.length;i++){
            for(int j = 0 ; j< camino.length;j++){
                suma += instancia.getDistancia(i, j)*instancia.getFlujo1(camino[i], camino[j]);
            }
        }
        hormiga.setEvaluacion1(suma);
        
        suma = 0;
        for(int i = 0; i < camino.length;i++){
            for(int j = 0 ; j< camino.length;j++){
                suma += instancia.getDistancia(i, j)*instancia.getFlujo2(camino[i], camino[j]);
            }
        }
        hormiga.setEvaluacion2(suma);
    }
    
    private void actualizarFeromonasGlobal(Hormiga hormiga, double[] avg){
                
        double value = 0.0;
         
        int[] camino = hormiga.getCamino();
        for (int i = 0; i < camino.length; i++) {
            for(int j = 0; j < camino.length;j++){
                value = (1 - coefEvaporacion) * tablaFeromona.getValue(i, camino[i], j, camino[j]) + coefEvaporacion  /(avg[0] * avg[1]);
                tablaFeromona.setValue(i, camino[i], j, camino[j],value);
            }
        }
    }
    
    private double calcularTaoPrima(double[] avr) {

        return (1.0 / Math.sqrt(avr[0] * avr[1] * n) * 1.0);
    }
    
    private double[] calcularParetoAverage() {

        double a1 = 0.0;
        double a2 = 0.0;

        for (Hormiga h : pareto) {
            a1 += h.getEvaluacion1();
            a2 += h.getEvaluacion2();
        }
        return new double[]{a1 / (n*1.0), a2 / (n*1.0)};
    }
    
    private void actualizarConjuntoPareto(Hormiga hormiga){
        for(Hormiga h : pareto){
            if(h.cubre(hormiga)){
                return;
            }
        }
        HashSet<Hormiga> toRemove = new HashSet<>();
        for(Hormiga h : pareto){
            if(hormiga.cubre(h)){
                toRemove.add(h);
            }
        }
        
        for(Hormiga h : toRemove){
            pareto.remove(h);
        }
        pareto.add(hormiga);
        
    }
    
    private boolean existenEstadosNoVisitados(ArrayList<Integer> ubicacionesLibres){
        return ubicacionesLibres.size() != 0;
    }
    
    
    private Lugar seleccionarSiguienteEstado(Lugar origen, int nroHormiga, ArrayList<Integer> ubicacionesLibres, ArrayList<Integer> edificiosLibres) {
        double q = Math.random();
  
        if (q <= q0)
            return elegirMaximo(origen, nroHormiga,ubicacionesLibres, edificiosLibres);
        else
            return elegirProbabilistico(origen, nroHormiga,ubicacionesLibres, edificiosLibres);
    }
    
    
    private Lugar elegirMaximo(Lugar origen,int nroHormiga, ArrayList<Integer> ubicacionesLibres, ArrayList<Integer> edificiosLibres) {
        double actual;
        double mayor = -1;
        
        Lugar siguienteEstado = null;

        double lambda = calcularLambda(nroHormiga);
        for (int i = 0; i < ubicacionesLibres.size(); i++) {
            for(int j = 0; j <edificiosLibres.size() ; j++){
                Lugar aux = new Lugar(i,ubicacionesLibres.get(i), j,edificiosLibres.get(j));
                
                actual = evaluarFormula(origen,aux,lambda);
                if (actual >= mayor) {
                    mayor = actual;
                    siguienteEstado = aux;
                }
            }
        }
        
        
        return siguienteEstado;
    }


    private Lugar elegirProbabilistico(Lugar origen, int nroHormiga, ArrayList<Integer> ubicacionesLibres, ArrayList<Integer> edificiosLibres) {
        ArrayList<Double> evals = new ArrayList<Double>();
        
        double sumaEvals = 0;
        
        double eval = 0;
        
        
        double lambda = calcularLambda(nroHormiga);
        for (int i = 0; i < ubicacionesLibres.size(); i++) {
            for(int j = 0; j <edificiosLibres.size() ; j++){
                Lugar aux = new Lugar(ubicacionesLibres.get(i), edificiosLibres.get(j));                
                eval = evaluarFormula(origen,aux,lambda);

                evals.add(eval);
                sumaEvals += eval;
            }
        }
        
        Double rdn = Math.random();        
        double aux = 0.0;
        
        int posicionSiguienteEstado = 0;
        while(aux < rdn){
            eval = evals.get(posicionSiguienteEstado) / sumaEvals;   
            aux += eval;
            posicionSiguienteEstado++;
        }
        posicionSiguienteEstado--;
        int i = posicionSiguienteEstado/ubicacionesLibres.size();
        int j = posicionSiguienteEstado%ubicacionesLibres.size();
        return new Lugar(i,ubicacionesLibres.get(i), 
                j,edificiosLibres.get(j));
    }
    
    private double evaluarFormula(Lugar origen, Lugar destino, double lambda) {
        double visibilidad1;
        double visibilidad2;
        double r;
        
        visibilidad1 = (double) 1 / 
                (instancia.getDistancia(origen.getUbicacion(), destino.getUbicacion())
                    *instancia.getFlujo1(origen.getEdificio(), destino.getEdificio()));
        visibilidad2 = (double) 1 / 
                (instancia.getDistancia(origen.getUbicacion(), destino.getUbicacion())
                    *instancia.getFlujo2(origen.getEdificio(), destino.getEdificio()));

        r = tablaFeromona.getValue(origen.getUbicacion(),origen.getEdificio(),
                destino.getUbicacion(),destino.getEdificio())
                * Math.pow(visibilidad1, lambda * beta)
                * Math.pow(visibilidad2, (1 - lambda) * beta);
        if(new Double(r).equals(Double.NaN))
            r = -1;
        return r;
    }

    private double calcularLambda(int hormigaActual) {
        return (double) hormigaActual / (cantHormigas - 1)*1.0;
    }
    
    private void actualizarFeromonasPasoPaso(Lugar origen, Lugar destino) {
        double tau;
        tau = (1 - coefEvaporacion) * tablaFeromona.getValue(origen.getUbicacion(),origen.getEdificio(),
                destino.getUbicacion(),destino.getEdificio()) + coefEvaporacion * taoInicial;
        tablaFeromona.setValue(origen.getUbicacion(),origen.getEdificio(),
                destino.getUbicacion(),destino.getEdificio(),tau);
    }

    public HashSet<Hormiga> getPareto() {
        return pareto;
    }

    public void setPareto(HashSet<Hormiga> pareto) {
        this.pareto = pareto;
    }
    
}
