/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.tarea4;

import java.util.Arrays;

/**
 * 
 * @author Marcelo Cardozo
 */
public class Hormiga {
    
    int[] camino;

    double evaluacion1;
    double evaluacion2;
    
    public static final int INDEFINIDO = -1;
    
    public Hormiga() {
    }
    public Hormiga(int cantidadLocalidades) {
        this.camino = new int[cantidadLocalidades];
        
        for(int i = 0 ; i< cantidadLocalidades ; i++ )
            camino[i] = INDEFINIDO;
    }

    public int[] getCamino() {
        return camino;
    }

    public void setCamino(int[] camino) {
        this.camino = camino;
    }
    
    public double getEvaluacion1() {
        return evaluacion1;
    }

    public void setEvaluacion1(double evaluacion1) {
        this.evaluacion1 = evaluacion1;
    }

    public double getEvaluacion2() {
        return evaluacion2;
    }

    public void setEvaluacion2(double evaluacion2) {
        this.evaluacion2 = evaluacion2;
    }

    @Override
    public String toString() {
        /*String s = "";
        for(int i =0; i < camino.length; i++){
            s+=camino[i]+";";
        }
        return s;*/
        return evaluacion1+";;"+evaluacion2;
    }
    
    public boolean cubre(Hormiga h2){
        return (this.evaluacion1<=h2.evaluacion1 && this.evaluacion2<=h2.evaluacion2);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Hormiga other = (Hormiga) obj;
        if (!Arrays.equals(this.camino, other.camino)) {
            return false;
        }
        return true;
    }
    
    
}
