/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.tarea4;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashSet;
import java.util.Set;

/**
 * 
 * @author Marcelo Cardozo
 */
public class Inicio {
    
    private static Instancia instancia;
   
    public static void main(String[] args) throws Exception{
        
        // para ejecutar, estar en directorio de jar, las instancias deben de estar en el actual directorio
        // java -cp tarea.jar com.mycompany.tarea4.Inicio SPEA 1 10
        // parametros: 1- accion; 2-cual instancia(1 o 2); 3- cantidad de ejecuciones
        /* ACCIONES
            SPEA- REALIZA EL ALGORITMO SPEA
            MOACS - REALIZA EL ALGORITMO MOACS
            YTRUE - ACTUALIZA EL YTRUE
            METRICAS - CALCULA E IMPRIME LAS METRICAS
        */
        if(args.length == 0)
            return;
        
        String alg = args[0];
        
        int nroInstancia = 1;
        int nroEjecuciones = 0;
        
        if(args.length == 3){
            nroInstancia = Integer.parseInt(args[1]);
            nroEjecuciones = Integer.parseInt(args[2]);
        }
        /*String alg = "METRICAS";
        int nroInstancia = 2;
        int nroEjecuciones = 5;
        */
        instancia = new Instancia("instancia"+nroInstancia+".txt");
        switch(alg){
            case "SPEA":
                spea(nroEjecuciones);
                break;
            case "MOACS":
                moacs(nroEjecuciones);
                break;
            case "YTRUE":
                actualizarYtrue();
                break;
            case "METRICAS":
                calcularMetricas();
                break;
        }
    }
    
    private static void spea(int nroEjecuciones){
        System.out.println("spea");
        SPEA spea = new SPEA(instancia, 100, 60, 100, 0.1);
        long[] tiempoSpea = new long[nroEjecuciones];
        for (int i = 0; i < nroEjecuciones; i++) {
            long time_start, time_end;
            time_start = System.currentTimeMillis();
            spea.run();
            
            time_end = System.currentTimeMillis();
            tiempoSpea[i] = time_end - time_start;
            
//            Set<Individuo> frente =spea.getPareto(); 
//            System.out.println("Iteracion "+i+"_Cantidad Elementos Frente: "+frente.size());
//            for(Individuo h: frente){
//                System.out.println(h.eval1+";;"+h.eval2);
//            }
            
            //guardarParetoSPEA(spea.getPareto());
            //Merge del pareto solucion de ahora, con el total de SPEA
            //para tener un REFERENTE de SPEA
            HashSet<Individuo> paretoTotalSPEA = getParetoTotalSPEA();
            mergePareto(paretoTotalSPEA, (HashSet<Individuo>) spea.getPareto());
            guardarParetoTotalSPEA(paretoTotalSPEA);
                        
        }
        
        System.out.println("Tiempo Promedio: " + promedio(tiempoSpea));
    }
    
    private static void moacs(int nroEjecuciones){
        HashSet<Individuo> paretoIndividuos = new HashSet<Individuo>();

        System.out.println("MOACS");
        MOACS moacs = new MOACS(instancia,5);
        
        long[] tiempoMOACS = new long[nroEjecuciones];
        for (int i = 0; i < nroEjecuciones; i++) {
            
            long time_start, time_end;
            time_start = System.currentTimeMillis();
            moacs.doWork();
            time_end = System.currentTimeMillis();
            
            tiempoMOACS[i] = time_end - time_start;
            
            
            Set<Hormiga> paretoHormigas = moacs.getPareto(); 
//            System.out.println("Iteracion "+i+"_Cantidad Elementos Frente: "+frente.size());
//            for(Hormiga h: frente){
//                System.out.println(h);
//            }
//            
            //"Convierte" a pareto de individuos
            for(Hormiga h: paretoHormigas){
                paretoIndividuos.add(new Individuo(h));
            }
            
            //Merge del pareto solucion de ahora, con el total de MOACS
            //para tener un REFERENTE de MOACS
            HashSet<Individuo> paretoTotalMOACS = getParetoTotalMOACS();
            mergePareto(paretoTotalMOACS, paretoIndividuos);
            guardarParetoTotalMOACS(paretoTotalMOACS);
            
            moacs.reset();
        }
        
    }
    
    
    public static double metricaDistancia(Set<Individuo> pareto, Set<Individuo> frente){
        double suma = 0;
        for(Individuo indFrente : frente){
            double menor = Double.POSITIVE_INFINITY;
            for(Individuo indPareto: pareto){
                double valor = distancia(indFrente, indPareto);
                if(menor > valor)
                    menor = valor;
            }
            suma += menor;
        }
        
        return (double) suma/frente.size();
    }
    public static double metricaDistribucion(Set<Individuo> pareto,double rho){
        if(pareto.size() <= 1)
            return 0;
        int cont = 0;
        for(Individuo ind1: pareto){
            for(Individuo ind2: pareto){
                if(distancia(ind1, ind2) > rho)
                    cont++;
            }
        }
        return (double) cont/(pareto.size()-1);
    }
    
    public static double metricaExtension(Set<Individuo> pareto){
        double mayor = Double.NEGATIVE_INFINITY;
        for(Individuo ind1: pareto){
            for(Individuo ind2: pareto){
                double valor = Math.abs(ind1.eval1-ind2.eval1)+Math.abs(ind1.eval2-ind2.eval2);
                if(valor > mayor)
                    mayor = valor;
            }
        }
        return Math.sqrt(mayor);
    }
    
    public static double distancia(Individuo ind1, Individuo ind2){
        return Math.sqrt(Math.pow((ind1.eval1-ind2.eval1), 2)+Math.pow((ind1.eval2-ind2.eval2), 2));
    }
    
    public static double metricaError(Set<Individuo> pareto, Set<Individuo> frente){
        int cont = 0;
        
        for(Individuo indFrente : frente){
            if(pareto.contains(indFrente))
                cont++;
        }
        
        return 1- (double)cont/frente.size();
    }
    
    private static void guardarParetoSPEA(Set<Individuo> pareto) {
        BufferedWriter writer = null;
        try {
            File paretoFile = new File("pareto_spea_"+instancia.getNombre()+".txt");
            writer = new BufferedWriter(new FileWriter(paretoFile,true));
            writer.write("//////////////////\n");
            for (Individuo ind : pareto){
                writer.write(ind.cromosoma+";"+ind.eval1+";"+ind.eval2+";"+"\n");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                // Close the writer regardless of what happens...
                writer.close();
            } catch (Exception e) {
            }
        }
    }
    
    private static void guardarParetoMOACS(Set<Hormiga> pareto) {
        BufferedWriter writer = null;
        try {
            File paretoFile = new File("pareto_moac_"+instancia.getNombre()+".txt");
            writer = new BufferedWriter(new FileWriter(paretoFile,true));
            writer.write("//////////////////\n");
            for (Hormiga h : pareto){
                String s = "[";
                for(int x : h.camino){
                    s += x+", ";
                }
                s = s.substring(0, s.length()-1);
                s+="]";
                writer.write(s+";"+h.evaluacion1+";"+h.evaluacion2+";"+"\n");
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                // Close the writer regardless of what happens...
                writer.close();
            } catch (Exception e) {
            }
        }
    }
    
    private static long promedio(long[] tiempos) {
        long sum = 0;
        for (int i = 0; i < tiempos.length; i++) 
            sum += tiempos[i];
            
        return sum/tiempos.length;
    }
    
    private static void mergePareto(HashSet<Individuo> pareto, HashSet<Individuo> nuevos) {
        for (Individuo ind : nuevos) {
            actualizarConjuntoPareto(pareto, ind);
        }
    }
    
    private static void actualizarConjuntoPareto(HashSet<Individuo> pareto, Individuo ind){
        for(Individuo i : pareto){
            if(i.cubre(ind)){
                return;
            }
        }
        HashSet<Individuo> toRemove = new HashSet<>();
        for(Individuo i : pareto){
            if(ind.cubre(i)){
                toRemove.add(i);
            }
        }
        
        for(Individuo i : toRemove){
            pareto.remove(i);
        }
        pareto.add(ind);
    }
    
    private static HashSet<Individuo> getParetoTotalSPEA() {
        try {
            FileInputStream fin = new FileInputStream("paretoTotalSPEA"+"-"+instancia.getNombre()+".hs");
            ObjectInputStream ois = new ObjectInputStream(fin);
            return (HashSet<Individuo>) ois.readObject();
        } catch (FileNotFoundException fnfe) {
            return new HashSet<Individuo>();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    
    private static HashSet<Individuo> getParetoTotalMOACS() {
        try {
            FileInputStream fin = new FileInputStream("paretoTotalMOACS"+"-"+instancia.getNombre()+".hs");
            ObjectInputStream ois = new ObjectInputStream(fin);
            return (HashSet<Individuo>) ois.readObject();
        } catch (FileNotFoundException fnfe) {
            return new HashSet<Individuo>();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    
    private static HashSet<Individuo> getYtrue() {
        try {
            FileInputStream fin = new FileInputStream("ytrue"+"-"+instancia.getNombre()+".hs");
            ObjectInputStream ois = new ObjectInputStream(fin);
            return (HashSet<Individuo>) ois.readObject();
        } catch (FileNotFoundException fnfe) {
            return new HashSet<Individuo>();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    
    private static void guardarYtrue(HashSet<Individuo> ytrue) {
        try {
            FileOutputStream fout = new FileOutputStream("ytrue"+"-"+instancia.getNombre()+".hs");
            ObjectOutputStream oos = new ObjectOutputStream(fout);
            oos.writeObject(ytrue);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private static void actualizarYtrue() {
        HashSet<Individuo> ytrue = getYtrue();
        HashSet<Individuo> paretoTotalSPEA = getParetoTotalSPEA();
        HashSet<Individuo> paretoTotalMOACS = getParetoTotalMOACS();
        mergePareto(ytrue, paretoTotalSPEA);
        mergePareto(ytrue, paretoTotalMOACS);
        guardarYtrue(ytrue);
    }
    
    private static void guardarParetoTotalSPEA(HashSet<Individuo> paretoTotal) {
        try {
            FileOutputStream fout = new FileOutputStream("paretoTotalSPEA"+"-"+instancia.getNombre()+".hs");
            ObjectOutputStream oos = new ObjectOutputStream(fout);
            oos.writeObject(paretoTotal);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private static void guardarParetoTotalMOACS(HashSet<Individuo> paretoTotal) {
        try {
            FileOutputStream fout = new FileOutputStream("paretoTotalMOACS"+"-"+instancia.getNombre()+".hs");
            ObjectOutputStream oos = new ObjectOutputStream(fout);
            oos.writeObject(paretoTotal);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private static void calcularMetricas() {
        double m1SPEA =0,m2SPEA=0,m3SPEA=0,errorSPEA=0;
        
        HashSet<Individuo> ytrue = getYtrue();
        HashSet<Individuo> paretoTotalSPEA = getParetoTotalSPEA();
        HashSet<Individuo> paretoTotalMOACS = getParetoTotalMOACS();
        
        m1SPEA = metricaDistancia(ytrue,paretoTotalSPEA);
        System.out.println("Distancia frente SPEA: "+m1SPEA);

        m3SPEA = metricaExtension(paretoTotalSPEA);
        System.out.println("Extension frente SPEA: "+m3SPEA);

        m2SPEA = metricaDistribucion(paretoTotalSPEA, m3SPEA/100);
        System.out.println("Distribucion frente SPEA: "+m2SPEA);
        
        errorSPEA = metricaError(ytrue, paretoTotalSPEA);
        System.out.println("Error frente SPEA: "+errorSPEA);

        double m1MOACS =0,m2MOACS=0,m3MOACS=0,errorMOACS=0;
        
        m1MOACS = metricaDistancia(ytrue,paretoTotalMOACS);
        System.out.println("Distancia frente MOACS: "+m1MOACS);

        m3MOACS = metricaExtension(paretoTotalMOACS);
        System.out.println("Extension frente MOACS: "+m3MOACS);
        
        m2MOACS = metricaDistribucion(paretoTotalMOACS, m3MOACS/100);
        System.out.println("Distribucion frente MOACS: "+m2MOACS);

        errorMOACS = metricaError(ytrue, paretoTotalMOACS);
        System.out.println("Error frente MOACS: "+errorMOACS);
    }
}
